
/**
 * Write a description of class ConverterCalc here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ConverterCalc
{
    // instance variables - replace the example below with your own
    private double result;

    /**
     * Constructor for objects of class ConverterCalc
     */
    public ConverterCalc(){

    }
    
    public double toAUD(double num1){
        result = num1 * 0.97;
        return result;
    }
    
    public double toUSD(double num1){
        result = num1 * 0.67;
        return result;
    }
    
    public double toGBP(double num1){
        result = num1 * 0.49;
        return result;
    }
    
    public double toJPY(double num1){
        result = num1 * 7.17;
        return result;
    }
    
    public double toEUR(double num1){
        result = num1 * 0.58;
        return result;
    }
    
    public double toIRR(double num1){
        result = num1 * 28218.78;
        return result;
    }
}
