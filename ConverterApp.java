
import java.util.Scanner;


public class ConverterApp
{
    Scanner input;
    ConverterCalc convert;
    double userValue = 0;
    double userMoney = 0;
    
    

    public ConverterApp(){
        input = new Scanner(System.in);
        convert = new ConverterCalc();
    }

    public void start(){
        System.out.println("\nHow many NZD do you want to convert?");
        
        try{
            getUserMoney();
        }
        catch(Exception e){
            System.out.println("Why would you try that?");
            System.exit(0);
        }
        
        System.out.println("\nChoose which currency to convert to:\n");
        System.out.println("1. Convert to AUD");
        System.out.println("2. Convert to USD");
        System.out.println("3. Convert to GBP");
        System.out.println("4. Convert to JPY");
        System.out.println("5. Convert to EUR");
        System.out.println("6. Convert to IRR");
        
        try{
            getInput();
        }
        catch(Exception e){
            System.out.println("Why would you try that?");
            System.exit(0);
        }
        
        
        doConversion();
        
        start();
    }
    
    void getInput(){
        userValue = input.nextDouble();
    }
    
    void getUserMoney(){
        userMoney = input.nextDouble();
    }
    
    void doConversion(){
        double result = 0;

        if(userValue == 1){
            result = convert.toAUD(userMoney);
            System.out.println(result);
        }
        else if(userValue == 2){
            result = convert.toUSD(userMoney);
            System.out.println(result);
        }
        else if(userValue == 3){
            result = convert.toGBP(userMoney);
            System.out.println(result);
        }
        else if(userValue == 4){
            result = convert.toJPY(userMoney);
            System.out.println(result);
        }
        else if(userValue ==5){
            result = convert.toEUR(userMoney);
            System.out.println(result);
        }
        else if(userValue == 6){
            result = convert.toIRR(userMoney);
            System.out.println(result);
        }
        else{
           System.out.println("Why would you try that?"); 
        }
    }
}
